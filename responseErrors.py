from flask import jsonify


def responseNotFound(message, error=None):
    messageResp = {'message': message, 'status': 404}
    response = jsonify(messageResp)
    response.status_code = 404
    return response

def responseError(message, error=None):
    messageResp = {'message': message, 'status': 400}
    response = jsonify(messageResp)
    response.status_code = 400
    return response