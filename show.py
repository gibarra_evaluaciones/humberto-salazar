from flask import Flask, jsonify, request
import requests
import responseErrors
import dbConnection as dbase
import time


app = Flask(__name__)
db = dbase.dbConection()
baseUrlApiTVMaze = 'https://api.tvmaze.com/'


@app.route('/search/<query>', methods=['GET'])
def search(query):
    apiURL = baseUrlApiTVMaze + 'search/shows?q=' + query
    data = requests.get(apiURL)
    if data.status_code == 200:
        data = data.json()
        if len(data) > 0:
            data = map(formatShowData, data)
            return jsonify(list(data))
        return responseErrors.responseNotFound('Not found')
    return responseErrors.responseError('An error occurred while searching')

def formatShowData(show):
    showid = int(show['show']['id'])  

    return {
        'id' : showid, 
        'name' : show['show']['name'], 
        'channel' : (show['show']['network']['name'] if show['show']['network'] else show['show']['webChannel']['name'] if show['show']['webChannel'] else ''), 
        'summary' : show['show']['summary'], 
        'genres' : show['show']['genres'],
        'comments': getComments(showid)
        }

@app.route('/show/<id>', methods=['GET'])
def show(id):
    shows = db['show']
    show = shows.find_one({'id': int(id)}, {'_id': False})

    if not show:
        apiURL = baseUrlApiTVMaze + 'shows/' + id
        data = requests.get(apiURL)
        if data.status_code == 200:
            dataShow = data.json()
            shows.insert_one(dataShow)
            show = data.json()
        elif data.status_code == 404:
            return responseErrors.responseNotFound('Not found')
        else:
            return responseErrors.responseError('An error occurred while searching')     

    show['comments'] = getComments(id)

    return jsonify(show)

@app.route('/comments', methods=['POST'])
def comments():
    id = request.json['id']
    comment = request.json['comment']
    rating = request.json['rating']

    if type(rating) != int or (int(rating) < 0 or int(rating) > 5):
        return responseErrors.responseError("The rating must be a number between 0 and 5.")

    comments = db['comment']
    if comments.insert_one({ 'show_id': id, 'comment': comment, 'rating': rating }):
        return jsonify({'message': 'Comment added', 'status': 200})
    else:
        return responseErrors.responseError('An error occurred while saving')  

@app.route('/rating/<showid>', methods=['GET'])
def rating(showid):
    time.sleep(4)
    comments = getComments(showid)
    numComments = len(comments)

    if numComments == 0:
        return jsonify({'rating_average': 'n/a'})

    ratingAvg = sum(c['rating'] for c in comments) / numComments
    return jsonify({'rating_average': ratingAvg})
        
def getComments(showid):
    comments = db['comment']
    showComments = comments.find({'show_id': int(showid)}, {'_id': 0, 'show_id': 0}) 
    return list(showComments)


if __name__ == '__main__':
    app.run(debug=True)